class appConfig {
  String appTitle = 'WaliedCheetos - IML (BYOD)';

  String accessKeyId = "***";
  String accessKeySecret = "***";

  String apikey = '';
  String token = "***";
  final herePlatform_CatalogHRN_BYOD_01 =
      "hrn:here:data::olp-here:heresdk-byod";
  final herePlatform_LayerID_BYOD_01 = "evchargingstations";

  double initialLAT = 25.40279;
  double initialLNG = 55.44262;

  double initialDistanceToEarthInMeters = 80000;
}
