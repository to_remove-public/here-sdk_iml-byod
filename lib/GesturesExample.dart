/*
 * Copyright (C) 2019-2023 HERE Europe B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 * License-Filename: LICENSE
 */

import 'dart:io';

import 'package:here_sdk/core.dart';
import 'package:here_sdk/gestures.dart';
import 'package:here_sdk/mapview.dart';

//**WaliedCheetos */
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'AppConfig.dart';
import 'fetchData.dart';
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;
//**WaliedCheetos */

// A callback to notify the hosting widget.
typedef ShowDialogFunction = void Function(String title, String message);

class GesturesExample {
  final HereMapController _hereMapController;
  final ShowDialogFunction _showDialog;

  //**WaliedCheetos */
  appConfig _appConfig = appConfig();
  MapImage? _mapImageMarker;
  MapMarker? _mapMarker;
  List<MapMarker> _mapMarkerList = [];
  //**WaliedCheetos */

  GesturesExample(ShowDialogFunction showDialogCallback,
      HereMapController hereMapController)
      : _showDialog = showDialogCallback,
        _hereMapController = hereMapController {
    double distanceToEarthInMeters = _appConfig.initialDistanceToEarthInMeters;

    MapMeasure mapMeasureZoom =
        MapMeasure(MapMeasureKind.distance, distanceToEarthInMeters);

    _hereMapController.camera.lookAtPointWithMeasure(
        GeoCoordinates(_appConfig.initialLAT, _appConfig.initialLNG),
        mapMeasureZoom);

    //**WaliedCheetos */
    _setPanGestureHandler();
    //**WaliedCheetos */

    _setTapGestureHandler();
    _setDoubleTapGestureHandler();
    _setTwoFingerTapGestureHandler();
    _setLongPressGestureHandler();

    _showDialog(
        "Gestures Example",
        "Pan, Shows Tap, DoubleTap, TwoFingerTap and LongPress gesture handling. " +
            "See log for details.");
  }

//**WaliedCheetos */
  void _setPanGestureHandler() {
    try {
      _hereMapController.gestures.panListener = PanListener(
          (GestureState gestureState, Point2D origin, Point2D translation,
              double velocity) {
        if (gestureState == GestureState.end) {
          //remove previous IML features markers from the map view
          //clear map

          //get new (current) map view BBox
          //ex.
          //bbox=55.055219,25.038425,55.508405,25.310064

          var bbox_southWestCorner =
              _toString(_hereMapController.camera.boundingBox?.southWestCorner);
          var bbox_northEastCorner =
              _toString(_hereMapController.camera.boundingBox?.northEastCorner);

          var bbox_southWestCorner_Reversed = _toString_Reverse(
              _hereMapController.camera.boundingBox?.southWestCorner);
          var bbox_northEastCorner_Reversed = _toString_Reverse(
              _hereMapController.camera.boundingBox?.northEastCorner);

          var herePlatform_CatalogHrn =
              _appConfig.herePlatform_CatalogHRN_BYOD_01;
          var herePlatform_LayerID = _appConfig.herePlatform_LayerID_BYOD_01;

          print(
              'New/Current map view BBox - southWestCorner: $bbox_southWestCorner - northEastCorner: $bbox_northEastCorner');

          //fetch/query IML features at the new (current) map view BBox
          //ex.
          //https://interactive.data.api.platform.here.com/interactive/v1/catalogs/hrn:here:data::olp-here:heresdk-byod/layers/sample-points/bbox?bbox=55.055219,25.038425,55.508405,25.310064

          // var imlURL =
          //     'https://interactive.data.api.platform.here.com/interactive/v1/catalogs/$_appConfig.herePlatform_CatalogHRN_BYOD_01/layers/$_appConfig.herePlatform_LayerID_BYOD_01/bbox?bbox=$bbox_southWestCorner_Reversed,$bbox_northEastCorner_Reversed';
          var imlURL =
              'https://interactive.data.api.platform.here.com/interactive/v1/catalogs/$herePlatform_CatalogHrn/layers/$herePlatform_LayerID/bbox?bbox=$bbox_southWestCorner_Reversed,$bbox_northEastCorner_Reversed';
          print(imlURL);
          _fetchData_withBearerToken(imlURL, _appConfig.token);

          //loop through the new features fetched at the new (current) map view BBox to the map view

          //add the new features fetched at the new (current) map view BBox to the map view
        } else {}
      });
    } catch (exception) {
      throw new Exception(exception);
    }
  }

  void _fetchData_withBearerToken(url, token) async {
    try {
      final response = await sendGetRequestWithBearerToken(url, token);

      // Handle the response data here
      print('Response status code: ${response.statusCode}');
      print('Response body: ${response.body}');

      // Parse the JSON response into a List
      final features = (jsonDecode(response.body)['features']) as List;

      if (features.isEmpty) {
        return;
      }
      _clearMapMarkers();
      // Loop through the List
      for (final feature in features) {
        final id = feature['id'];
        final type = feature['type'];
        final props = jsonEncode(feature['properties']);

        // You can now work with the individual properties of each item
        print('ID: $id, Type: $type');

        Metadata metadata = new Metadata();
        metadata.setString("properties", props);

        _addMarker(
            GeoCoordinates(feature['geometry']['coordinates'][1],
                feature['geometry']['coordinates'][0]),
            1,
            metadata);
      }
    } catch (e) {
      // Handle any exceptions or errors
      print('Error: $e');
    }
  }

  String _toString_Reverse(GeoCoordinates? geoCoordinates) {
    if (geoCoordinates == null) {
      // This can happen, when there is no map view touched, for example, when the screen was tilted and
      // the touch point is on the horizon.
      return "Error: No valid geo coordinates.";
    }

    return geoCoordinates.longitude.toString() +
        "," +
        geoCoordinates.latitude.toString();
  }

  Future<Uint8List> _loadFileAsUint8List(String assetPathToFile) async {
    // The path refers to the assets directory as specified in pubspec.yaml.
    ByteData fileData = await rootBundle.load(assetPathToFile);
    return Uint8List.view(fileData.buffer);
  }

  Future<void> _addMarker(GeoCoordinates geoCoordinates, int drawOrder,
      Metadata markerMetaData) async {
    // Reuse existing MapImage for new map markers.
    if (_mapImageMarker == null) {
      Uint8List imagePixelData =
          await _loadFileAsUint8List('assets/icons/poi_EVChargingStation.png');
      _mapImageMarker =
          MapImage.withPixelDataAndImageFormat(imagePixelData, ImageFormat.png);
    }

    // By default, the anchor point is set to 0.5, 0.5 (= centered).
    // Here the bottom, middle position should point to the location.
    Anchor2D anchor2D = Anchor2D.withHorizontalAndVertical(0.5, 1);

    MapMarker mapMarker =
        MapMarker.withAnchor(geoCoordinates, _mapImageMarker!, anchor2D);
    mapMarker.drawOrder = drawOrder;
    mapMarker.metadata = markerMetaData;

    _hereMapController.mapScene.addMapMarker(mapMarker);
    _mapMarkerList.add(mapMarker);
  }

  void _clearMapMarkers() {
    try {
      for (var mapMarker in _mapMarkerList) {
        _hereMapController.mapScene.removeMapMarker(mapMarker);
      }
      _mapMarkerList.clear();
    } catch (exception) {
      throw new Exception(exception);
    }
  }

  void _pickMapMarker(Point2D touchPoint) {
    double radiusInPixel = 2;
    _hereMapController.pickMapItems(touchPoint, radiusInPixel,
        (pickMapItemsResult) {
      if (pickMapItemsResult == null) {
        // Pick operation failed.
        return;
      }

      // Note that MapMarker items contained in a cluster are not part of pickMapItemsResult.markers.
      // _handlePickedMapMarkerClusters(pickMapItemsResult);

      // Note that 3D map markers can't be picked yet. Only marker, polgon and polyline map items are pickable.
      List<MapMarker> mapMarkerList = pickMapItemsResult.markers;
      int listLength = mapMarkerList.length;
      if (listLength == 0) {
        print("No map markers found.");
        return;
      }

      MapMarker topmostMapMarker = mapMarkerList.first;
      Metadata? metadata = topmostMapMarker.metadata;
      if (metadata != null) {
        String message =
            metadata.getString("properties") ?? "No message found.";

        _showDialog("Map Marker picked", message);
        return;
      }

      _showDialog("Map Marker picked", "No metadata attached.");
    });
  }

  void _handlePickedMapMarkerClusters(PickMapItemsResult pickMapItemsResult) {
    List<MapMarkerClusterGrouping> groupingList =
        pickMapItemsResult.clusteredMarkers;
    if (groupingList.length == 0) {
      return;
    }

    MapMarkerClusterGrouping topmostGrouping = groupingList.first;
    int clusterSize = topmostGrouping.markers.length;
    if (clusterSize == 0) {
      // This cluster does not contain any MapMarker items.
      return;
    }
    if (clusterSize == 1) {
      _showDialog("Map marker picked", "This MapMarker belongs to a cluster.");
    } else {
      int totalSize = topmostGrouping.parent.markers.length;
      _showDialog(
          "Map marker cluster picked",
          "Number of contained markers in this cluster: $clusterSize." +
              "Total number of markers in this MapMarkerCluster: $totalSize.");
    }
  }
//**WaliedCheetos */

  void _setTapGestureHandler() {
    _hereMapController.gestures.tapListener = TapListener((Point2D touchPoint) {
      var geoCoordinates =
          _toString(_hereMapController.viewToGeoCoordinates(touchPoint));
      print('Tap at: $geoCoordinates');
      _pickMapMarker(touchPoint);
    });
  }

  void _setDoubleTapGestureHandler() {
    _hereMapController.gestures.doubleTapListener =
        DoubleTapListener((Point2D touchPoint) {
      var geoCoordinates =
          _toString(_hereMapController.viewToGeoCoordinates(touchPoint));
      print('DoubleTap at: $geoCoordinates');
    });
  }

  void _setTwoFingerTapGestureHandler() {
    _hereMapController.gestures.twoFingerTapListener =
        TwoFingerTapListener((Point2D touchCenterPoint) {
      var geoCoordinates =
          _toString(_hereMapController.viewToGeoCoordinates(touchCenterPoint));
      print('TwoFingerTap at: $geoCoordinates');
    });
  }

  void _setLongPressGestureHandler() {
    _hereMapController.gestures.longPressListener =
        LongPressListener((GestureState gestureState, Point2D touchPoint) {
      var geoCoordinates =
          _toString(_hereMapController.viewToGeoCoordinates(touchPoint));

      if (gestureState == GestureState.begin) {
        print('LongPress detected at: $geoCoordinates');
      }

      if (gestureState == GestureState.update) {
        print('LongPress update at: $geoCoordinates');
      }

      if (gestureState == GestureState.end) {
        print('LongPress finger lifted at: $geoCoordinates');
      }

      if (gestureState == GestureState.cancel) {
        print(
            'Map view lost focus. Maybe a modal dialog is shown or the app is sent to background.');
      }
    });
  }

  String _toString(GeoCoordinates? geoCoordinates) {
    if (geoCoordinates == null) {
      // This can happen, when there is no map view touched, for example, when the screen was tilted and
      // the touch point is on the horizon.
      return "Error: No valid geo coordinates.";
    }

    return geoCoordinates.latitude.toString() +
        "," +
        geoCoordinates.longitude.toString();
  }
}
