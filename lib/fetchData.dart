import 'package:http/http.dart' as http;

Future<http.Response> sendGetRequestWithBearerToken(
    String url, String token) async {
  // Create the headers with Bearer token authorization
  final headers = <String, String>{
    'Authorization': 'Bearer $token',
  };

  try {
    // Send the GET request
    final response = await http.get(Uri.parse(url), headers: headers);

    // Check the response status code and handle it as needed
    if (response.statusCode == 200) {
      // Successful response, return it
      return response;
    } else {
      // Handle error, you can throw an exception or return the response
      throw Exception(
          'Failed to load data, status code: ${response.statusCode}');
    }
  } catch (e) {
    // Handle network errors or other exceptions
    throw Exception('Failed to send GET request: $e');
  }
}
